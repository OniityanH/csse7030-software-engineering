import random

operator_list = ['+', '-', '*', '//', '**', '%']
key = 0
for i in range(10):
    operator = random.sample(operator_list, 1)
    number1 = random.randint(10, 99)
    if (operator != ['**']):
        number2 = random.randint(10, 99)
        if (operator == ['+']):
            key = number1 + number2
        elif (operator == ['-']):
            key = number1 - number2
        elif (operator == ['*']):
            key = number1 * number2
        elif (operator == ['//']):
            key = number1 // number2
        elif (operator == ['%']):
            key = number1 % number2
    else:
        number2 = random.randint(0, 3)
        key = number1 ** number2

    answer = int(input(str(number1) + ' ' + str(operator).strip("['']") + ' ' + str(number2) + " = "))
    if (answer == key ):
        print("Correct!\n")
    else:
        print("No, the correct answer is " + str(key) + "\n")