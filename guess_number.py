import random
print("Try to guess the number I am thinking of between 1 and 100.")
key = random.randrange(1, 100)
print(key)
guess = 0

while (guess != key):
    guess = int(input("\nPlease enter your guess:"))
    if (guess == key):
        print("Congratulations! You have guessed correctly.")
        break
    elif (guess == -1):
        print("The number you were trying to guess was " + str(key) + ".")
        break
    else:
        print("Sorry that is not correct.")
